<?php
session_start();

if(isset($_GET['id']) && !empty($_GET['id'])){
    require_once('connect.php');
    $id = strip_tags($_GET['id']);
    $sql = "SELECT `email`, `nb_facc_coins` FROM `user` WHERE `id`=:id;";

    $query = $db->prepare($sql);

    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();

    $user = $query->fetch();
    if(!$user){
        $_SESSION['erreur'] = "Cet id n'existe pas";
        header('Location: index.php');
    }
}else{
    $_SESSION['erreur'] = "URL invalide";
    header('Location: index.php');
}

if($_POST) {
    if(isset($_POST['email']) && !empty($_POST['email'])
        && isset($_POST['nb_facc_coins']) && !empty($_POST['nb_facc_coins'])) {
            // s'il n'y a pas d'erreur, on se connecte à la bdd
            require_once('connect.php');
            // on nettoie les données envoyées
            $email = strip_tags($_POST['email']);
            $nb_facc_coins = strip_tags($_POST['nb_facc_coins']);

            $sql = "INSERT INTO `transactions` (`date_creation`, `expediteur`, `destinataire`, `montant`) VALUES (:date_creation, :expediteur, :destinataire, :montant);";

            $query = $db->prepare($sql);
           
            $date_creation = date("Y-m-d H:i:s");
            $mail_expediteur = $user['email'];
            $query->bindValue(':date_creation', $date_creation, PDO::PARAM_STR);
            $query->bindValue(':expediteur', $mail_expediteur, PDO::PARAM_STR);
            $query->bindValue(':destinataire', $email, PDO::PARAM_STR);
            $query->bindValue(':montant', $nb_facc_coins, PDO::PARAM_STR);

            $query->execute();

            $sql2 = "UPDATE `user` SET `nb_facc_coins`=:nouveau_nb_facc_coins_expediteur WHERE `id`=:id;";
            $query2 = $db->prepare($sql2);
            $nouveau_nb_facc_coins_expediteur = $user['nb_facc_coins'] - $nb_facc_coins;
            $query2->bindValue(':id', $id, PDO::PARAM_INT);
            $query2->bindValue(':nouveau_nb_facc_coins_expediteur', $nouveau_nb_facc_coins_expediteur, PDO::PARAM_STR);

            $query2->execute();

            $sql3 = "SELECT `id`, `nb_facc_coins` FROM `user` WHERE `email`=:email;";
            $query3 = $db->prepare($sql3);
            $query3->bindValue(':email', $email, PDO::PARAM_STR);
            $query3->execute();
            $destinataire = $query3->fetch();
            $destinataire_id = $destinataire['id'];
            $ancien_nb_facc_coins_destinataire = $destinataire['nb_facc_coins'];

            $sql4 = "UPDATE `user` SET `nb_facc_coins`=:nouveau_nb_facc_coins_destinataire WHERE `id`=:id;";
            $query4 = $db->prepare($sql4);
            $nouveau_nb_facc_coins_destinataire = $ancien_nb_facc_coins_destinataire + $nb_facc_coins;
            $query4->bindValue(':id', $destinataire_id, PDO::PARAM_INT);
            $query4->bindValue(':nouveau_nb_facc_coins_destinataire', $nouveau_nb_facc_coins_destinataire, PDO::PARAM_STR);

            $query4->execute();

            $_SESSION['message'] = "Un montant de ".$nb_facc_coins." Facc coins a bien été envoyé à ".$email."";
            require_once('close.php');
            header('Location: index.php');
            
    }else{
        $_SESSION['erreur'] = "Le formulaire est incomplet";
    }
}


?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ajouter un utilisateur</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <main class="container">
            <div class="row">
                <section class="col-12">
                    <h1>Envoyer des Facc coins</h1>
                    <form method="post">
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="email" name="email" id="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="nb_facc_coins">Nombre de facc coins</label>
                            <input type="number" step="0.01" name="nb_facc_coins" id="nb_facc_coins" class="form-control">
                        </div>
                        <br>
                        <button class="btn btn-primary">Envoyer</button>
                    </form>
                    <br />
                    <a href="index.php">Retour</a>
                </section>
            </div>
        </main>
    </body>
</html>