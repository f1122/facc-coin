<?php
// On démarre une session
session_start();

// On inclut la connexion à la base
require_once('connect.php');

if(isset($_GET['id']) && !empty($_GET['id'])){
    // On nettoie l'id envoyée
    $id = strip_tags($_GET['id']);
    // On écrit notre requête
    $sql = 'SELECT * FROM `user` WHERE `id`=:id';

    // On prépare la requête
    $query = $db->prepare($sql);

    // On attache les valeurs
    $query->bindValue(':id', $id, PDO::PARAM_INT);

    // On exécute la requête
    $query->execute();

    // On stocke le résultat dans un tableau associatif
    $user = $query->fetch();

    if(!$user){
        $_SESSION['erreur'] = "Cet id n'existe pas";
        header('Location: index.php');
    }
}else{
    $_SESSION['erreur'] = "URL invalide";
    header('Location: index.php');
}

require_once('close.php');
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Détail utilisateur <?= $user['id'] ?></title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <main class="container">
            <div class="row">
                <section class="col-12">
                    <h1>Détails de l'utilisateur <?= $user['nom']." ". $user['prenom'] ?></h1>
                    <p>ID : <?= $user['id'] ?></p>
                    <p>Nom : <?= $user ['nom'] ?></p>
                    <p>Prénom : <?= $user['prenom'] ?></p>
                    <p>Email : <?= $user['email'] ?></p>
                    <p>Nombre de Facc Coins : <?= $user['nb_facc_coins'] ?></p>
                    <p><a href="edit.php?id=<?= $user['id'] ?>">Modifier</a>  <a href="delete.php?id=<?= $user['id'] ?>">Supprimer</a></p>
                    <p><a href="index.php">Retour</a></p>
                </section>
            </div>
        </main>
        
        
    </body>
</html>
