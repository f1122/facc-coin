<?php
session_start();

if($_POST) {
    if(isset($_POST['nom']) && !empty($_POST['nom'])
        && isset($_POST['prenom']) && !empty($_POST['prenom'])
        && isset($_POST['email']) && !empty($_POST['email'])
        && isset($_POST['nb_facc_coins']) && !empty($_POST['nb_facc_coins'])) {
            // s'il n'y a pas d'erreur, on se connecte à la bdd
            require_once('connect.php');
            // on nettoie les données envoyées
            $nom = strip_tags($_POST['nom']);
            $prenom = strip_tags($_POST['prenom']);
            $email = strip_tags($_POST['email']);
            $nb_facc_coins = strip_tags($_POST['nb_facc_coins']);

            $sql = "INSERT INTO `user` (`nom`, `prenom`, `email`, `nb_facc_coins`) VALUES (:nom, :prenom, :email, :nb_facc_coins);";

            $query = $db->prepare($sql);

            $query->bindValue(':nom', $nom, PDO::PARAM_STR);
            $query->bindValue(':prenom', $prenom, PDO::PARAM_STR);
            $query->bindValue(':email', $email, PDO::PARAM_STR);
            $query->bindValue(':nb_facc_coins', $nb_facc_coins, PDO::PARAM_STR);

            $query->execute();
            $_SESSION['message'] = "Utilisateur ajouté avec succès !";
            require_once('close.php');
            header('Location: index.php');
            
    }else{
        $_SESSION['erreur'] = "Le formulaire est incomplet";
    }
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ajouter un utilisateur</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    </head>
    <body>
        <main class="container">
            <div class="row">
                <section class="col-12">
                    <h1>Ajouter un utilisateur</h1>
                    <form method="post">
                        <div class="form-group">
                            <label for="nom">Nom</label>
                            <input type="text" name="nom" id="nom" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="prenom">Prenom</label>
                            <input type="text" name="prenom" id="prenom" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="email" name="email" id="email" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="nb_facc_coins">Nombre de facc coins</label>
                            <input type="number" step="0.01" name="nb_facc_coins" id="nb_facc_coins" class="form-control">
                        </div>
                        <br>
                        <button class="btn btn-primary">Enregistrer</button>
                    </form>
                    <br>
                    <a href="index.php">Retour</a>
                </section>
            </div>
        </main>
    </body>
</html>