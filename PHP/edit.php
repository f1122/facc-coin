<?php
session_start();
if($_POST){
    if(isset($_POST['id']) && !empty($_POST['id'])
        && isset($_POST['nom']) && !empty($_POST['nom'])
        && isset($_POST['prenom']) && !empty($_POST['prenom'])
        && isset($_POST['email']) && !empty($_POST['email'])
        && isset($_POST['nb_facc_coins']) && !empty($_POST['nb_facc_coins'])){
            require_once('connect.php');
            $id = strip_tags($_GET['id']);
            $nom = strip_tags($_POST['nom']);
            $prenom = strip_tags($_POST['prenom']);
            $email = strip_tags($_POST['email']);
            $nb_facc_coins = strip_tags($_POST['nb_facc_coins']);

            $sql = "UPDATE `user` SET `nom`=:nom, `prenom`=:prenom, `email`=:email, `nb_facc_coins`=:nb_facc_coins WHERE `id`=:id;";

            $query = $db->prepare($sql);

            $query->bindValue(':nom', $nom, PDO::PARAM_STR);
            $query->bindValue(':prenom', $prenom, PDO::PARAM_STR);
            $query->bindValue(':email', $email, PDO::PARAM_STR);
            $query->bindValue(':nb_facc_coins', $nb_facc_coins, PDO::PARAM_STR);
            $query->bindValue(':id', $id, PDO::PARAM_INT);

            $query->execute();
            $_SESSION['message'] = "Utilisateur modifié avec succès !";
            require_once('close.php');
            header('Location: index.php');
    }else{
        $_SESSION['erreur'] = "Le formulaire est incomplet";}
}
if(isset($_GET['id']) && !empty($_GET['id'])){
    require_once('connect.php');
    $id = strip_tags($_GET['id']);
    $sql = "SELECT * FROM `user` WHERE `id`=:id;";

    $query = $db->prepare($sql);

    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();

    $user = $query->fetch();
    if(!$user){
        $_SESSION['erreur'] = "Cet id n'existe pas";
        header('Location: index.php');
    }
}else{
    $_SESSION['erreur'] = "URL invalide";
    header('Location: index.php');
}

?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Modifier un utilisateur</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>
    <body>
        <main class="container">
            <div class="row">
                <section class="col-12">
                    <h1>Modifier un utilisateur</h1>
                    <form method="post">
                        <div class="form-group">
                            <label for="nom">Nom</label>
                            <input type="text" name="nom" id="nom" value="<?= $user['nom'] ?>" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="prenom">Prénom</label>
                            <input type="text" name="prenom" id="prenom" value="<?= $user['prenom'] ?>" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input type="email" name="email" id="email" value="<?= $user['email'] ?>" class="form-control">
                            </div>
                        <div class="form-group">
                            <label for="nb_facc_coins">Nombre de Facc Coins</label>
                            <input type="number" step="0.01" name="nb_facc_coins" id="nb_facc_coins" value="<?= $user['nb_facc_coins'] ?>" class="form-control">
                        </div>
                        <p>
                            <button class="btn btn-primary">Enregistrer</button>
                        </p>
                        <input type="hidden" name="id" value="<?= $user['id'] ?>">
                    </form>
                    <br />
                    <a href="index.php">Retour</a>
                </section>
            </div>
        </main>
    </body>
</html>