<?php
// On démarre une session
session_start();

// On inclut la connexion à la base
require_once('connect.php');

// On récupère la requête depuis le formulaire
$sql = 'SELECT * FROM `user`';

// On prépare la requête
$query = $db->prepare($sql);

// On exécute la requête
$query->execute();

// On stocke le résultat dans un tableau associatif
$result = $query->fetchAll(PDO::FETCH_ASSOC);

require_once('close.php');
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste des utilisateurs</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>
    <main class="container">
        <div class="row">
            <section class="col-12">
            <?php 
                if (!empty($_SESSION['erreur'])){
                    echo '<div class="alert alert-danger" role="alert">
                            '.$_SESSION['erreur'].'
                          </div>';
                          $_SESSION['erreur'] = "";
                }
            ?>    
             <?php 
                if (!empty($_SESSION['message'])){
                    echo '<div class="alert alert-success" role="alert">
                            '.$_SESSION['message'].'
                          </div>';
                          $_SESSION['message'] = "";
                }
            ?>    
            <h1>Liste des utilisateurs</h1>
                <table class="table">
                    <thead>
                        <th>ID</th>
                        <th>Nom</th>
                        <th>Prenom</th>
                        <th>Email</th>
                        <th>Nombre de facc_coins</th>
                    </thead>
                    <tbody>
                        <?php
                        // on boucle sur la variable result
                        foreach($result as $user){
                            ?>
                            <tr>
                                <td><?= $user['id'] ?></td>
                                <td><?= $user['nom'] ?></td>
                                <td><?= $user['prenom'] ?></td>
                                <td><?= $user['email'] ?></td>
                                <td><?= $user['nb_facc_coins'] ?><br><a href="send.php?id=<?= $user['id'] ?>">Envoyer</a></td>
                                <td><a href="details.php?id=<?= $user['id'] ?>">Voir</a>  <a href="edit.php?id=<?= $user['id'] ?>">Modifier</a>  <a href="delete.php?id=<?= $user['id'] ?>">Supprimer</a></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
                <a href="add.php" class="btn btn-primary">Ajouter un utilisateur</a>
            </section>
        </div>
    </main>
</body>
</html>
