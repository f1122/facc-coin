<?php
session_start();
if(isset($_GET['id']) && !empty($_GET['id'])){
    require_once('connect.php');
    $id = strip_tags($_GET['id']);
    $sql = "DELETE FROM `user` WHERE `id`=:id;";

    $query = $db->prepare($sql);

    $query->bindValue(':id', $id, PDO::PARAM_INT);
    $query->execute();
    $_SESSION['message'] = "Utilisateur supprimé de la bdd";
    require_once('close.php');
    header('Location: index.php');
}
